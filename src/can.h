////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2021, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
// These are frontend functions for the std peripheral library CAN functions.
//

#ifndef CAN_H
#define CAN_H

#include <stdint.h>
#include <stdbool.h>
#include <stm8s.h>
//#include <appconfig.h>

//Prescaler-constants
//According to reference manual 23.6.6 Bit timing
//Note: If you use these constants, decrement them by 1 before writing into register
//Why? See Reference manual
//typedef struct{
//  uint8_t brp;
//  uint8_t bs1;
//  uint8_t bs2;
//}canhelper_prescaler;
//
//typedef struct{
//  uint32_t id;
//  uint8_t dlc;
//  uint8_t data[8];
//}canmessage_t;
//
//typedef enum{
//  SPEED_10k,
//  SPEED_20k,
//  SPEED_50k,
//  SPEED_83k3,
//  SPEED_100k,
//  SPEED_125k,
//  SPEED_250k,
//  SPEED_500k,
//  SPEED_800k,
//  SPEED_1M
//}canhelper_speed_t;
//
//extern uint8_t can_sleep_state;

//void can_calculate_speedparameter(void);
//#if CAN_DEFAULT_SPEED == 10
//void can_calculate_speed_10k();
//#elif CAN_DEFAULT_SPEED == 20
//void can_calculate_speed_20k();
//#elif CAN_DEFAULT_SPEED == 50
//void can_calculate_speed_50k();
//#elif CAN_DEFAULT_SPEED == 83
//void can_calculate_speed_83k3();
//#elif CAN_DEFAULT_SPEED == 100
//void can_calculate_speed_100k();
//#elif CAN_DEFAULT_SPEED == 125
//void can_calculate_speed_125k();
//#elif CAN_DEFAULT_SPEED == 250
//void can_calculate_speed_250k();
//#elif CAN_DEFAULT_SPEED == 500
//void can_calculate_speed_500k();
//#elif CAN_DEFAULT_SPEED == 800
//void can_calculate_speed_800k();
//#elif CAN_DEFAULT_SPEED == 1000
//void can_calculate_speed_1M();
//#endif
void can_init(void);
void can_interrupt_enable(void);
bool can_send_message(uint32_t id,uint8_t dlc,uint8_t *data);

#endif
