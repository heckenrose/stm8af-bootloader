////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2021, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
// Bootloader configuration functions.
//

#include "appconfig.h"

#include <stddef.h>
#include <stm8s.h>
#include "config.h"
#include "eeprom.h"
#include "canmessages.h"

#include "ctrl_state.h"

//We use the coincidence, that addresses and CRC are both 16 bit.
//This saves us some bytes in the text section.
#define BOOTLOADER_APP_VALUE_CHUNK_LEN 2

typedef union{
  uint8_t data[BOOTLOADER_APP_VALUE_CHUNK_LEN];
  struct{
    uint16_t app_value;
  }content;
}app_value_chunk_t;

void config_change_to_bootloader_mode(void){
  //We need to prevent IVT relocation
  if(*(volatile uint8_t*)(BOOTLOADER_IVT_RELOC_ADDR) != 0x00){
    eeprom_write_byte(BOOTLOADER_IVT_RELOC_ADDR,0x00);
  }
}

bool config_get_sw_update_requested(void){
  //0x00 means "no SW update requested by app"
  if((*(volatile uint8_t*)(BOOTLOADER_SW_UPDATE_ADDR))==0x00){
    return false;
  }
  else return true;
}

void config_set_sw_update_requested(bool update_requested){
  if(update_requested==true){
    if(*(volatile uint8_t*)(BOOTLOADER_SW_UPDATE_ADDR) != 0x01){
      eeprom_write_byte(BOOTLOADER_SW_UPDATE_ADDR,0x01);
    }
  }
  else{
    if(*(volatile uint8_t*)(BOOTLOADER_SW_UPDATE_ADDR) != 0x00){
      eeprom_write_byte(BOOTLOADER_SW_UPDATE_ADDR,0x00);
    }
  }
}

///
/// Writes the value for a selected app value into EEPROM. Possible app values are
/// - BOOTLOADER_APP_END_ADDR
/// - BOOTLOADER_APP_CRC_ADDR
///
void config_write_app_value(uint16_t selected_app_value, uint16_t value){
  app_value_chunk_t chunk;

  chunk.content.app_value=value;
  //Now write to eeprom
  for(uint8_t i=0;i<BOOTLOADER_APP_VALUE_CHUNK_LEN;++i){
    *(volatile uint8_t*) (uint32_t)(selected_app_value+i) = chunk.data[i];
  }
}

///
/// Returns a selected app value from EEPROM. Possible app values are
/// - BOOTLOADER_APP_END_ADDR
/// - BOOTLOADER_APP_CRC_ADDR
///
uint16_t config_get_app_value(uint16_t selected_app_value){
  app_value_chunk_t chunk;

  for(uint8_t i=0;i<BOOTLOADER_APP_VALUE_CHUNK_LEN;++i){
    chunk.data[i]=(*(volatile uint8_t*)((uint32_t)selected_app_value+i));
  }
  return chunk.content.app_value;
}

uint16_t config_calculate_app_crc(void){
  //We need to calculate the crc with method CRC-CCITT over the flash memory,
  //starting at APP_START_ADD until APP_END_ADDR.
  //https://www.lammertbies.nl/comm/info/crc-calculation
  //Problem aktuell:
  //https://community.st.com/s/question/0D53W000016PMEASA4/read-access-to-flash-beyond-address-0x10000
  //Das ist mit derzeitigen Mitteln nicht zu lösen.
  //Es gibt dafür aber einen Feature-Request des SDCC:
  //https://sourceforge.net/p/sdcc/feature-requests/560/
  uint8_t x;
  uint16_t crc=0xFFFF;
  uint32_t flash_start_addr=APP_START_ADDR;
  uint32_t flash_end_addr=config_get_app_value(BOOTLOADER_APP_END_ADDR);
  uint32_t current_flash_addr;
  uint8_t current_byte;

  if((flash_end_addr<=0xFFFF)&&(flash_end_addr>flash_start_addr)){
    for(current_flash_addr=flash_start_addr;
        current_flash_addr<=flash_end_addr;++current_flash_addr){
      //Read byte from flash
      current_byte=(*(volatile uint8_t*)(current_flash_addr));
      x = crc >> 8 ^ current_byte;
      x ^= x>>4;
      crc = (crc << 8) ^ ((uint16_t)(x << 12)) ^ ((uint16_t)(x <<5)) ^ ((uint16_t)x);
    }
  }

  return crc;
}
