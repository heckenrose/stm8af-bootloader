////////////////////////////////////////////////////////////////////////////////
//
// MIT License
//
// Copyright (c) 2019 lujji
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#include "ram.h"
#include "stm8s.h"
#include "appconfig.h"

#pragma codeseg RAM_SEG
void ram_flash_write_block(uint16_t startaddress, const uint8_t *buf) {
  uint16_t i = 0;

  // enable flash block write
  FLASH->CR2 = FLASH_CR2_PRG;
  FLASH->NCR2 = (uint8_t)(~FLASH_NCR2_NPRG);

  // write data buffer
  for (i = 0; i < FLASH_BLOCK_SIZE; i++){
    *((volatile uint8_t*) (uint32_t)(startaddress+i)) = buf[i];
  }

  // wait for operation to complete
  while((FLASH->IAPSR & FLASH_IAPSR_EOP)!=FLASH_IAPSR_EOP);
}
