////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2021, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
#include "appconfig.h"
#include <stm8s.h>
#include "interruptvectors.h"

#include "can.h"
#include "canmessages.h"
#include "config.h"
#include "ctrl_state.h"
#include "eeprom.h"
#include "ram.h"

static volatile uint8_t f_ram[128];
static volatile uint8_t RAM_SEG_LEN;
static void (*flash_write_block)(uint16_t addr, const uint8_t *buf) =
        (void (*)(uint16_t, const uint8_t *)) f_ram;

/**
 * Write RAM_SEG section length into RAM_SEG_LEN
 */
inline void get_ram_section_length() {
  __asm__("mov _RAM_SEG_LEN, #l_RAM_SEG");
}

inline void clock_init(void){
  CLK->CKDIVR &= (uint8_t)(~CLK_CKDIVR_HSIDIV);
#if USE_MCU_CLOCK_MHZ == 16
  CLK->CKDIVR |= (uint8_t)CLK_PRESCALER_HSIDIV1;
#elif USE_MCU_CLOCK_MHZ == 8
  CLK->CKDIVR |= (uint8_t)CLK_PRESCALER_HSIDIV2;
#elif USE_MCU_CLOCK_MHZ == 4
  CLK->CKDIVR |= (uint8_t)CLK_PRESCALER_HSIDIV4;
#elif USE_MCU_CLOCK_MHZ == 2
  CLK->CKDIVR |= (uint8_t)CLK_PRESCALER_HSIDIV8;
#else
#error "please define a clock value in appconfig.h"
#endif
}

void bootloader_task(void){
  static uint32_t taskcounter=0;
  static uint32_t blocks_flashed=0;

  //watchdog_serve();  //We don't use watchdog yet.
  canmessages_rx_handler();
  if(ctrl_state_flush_to_flash==true){
    //Write received program data to flash
    uint32_t address_to_flash=APP_START_ADDR;
    address_to_flash+=blocks_flashed*FLASH_BLOCK_SIZE;

    //Unlock program memory
    FLASH->PUKR = FLASH_RASS_KEY1;
    FLASH->PUKR = FLASH_RASS_KEY2;

    flash_write_block((uint16_t)address_to_flash, ctrl_state_flash_data);
    ++blocks_flashed;

    //Clean up
    ctrl_state_clear_flash_data();
    //FLASH_Lock(FLASH_MEMTYPE_PROG);   //Locking is not needed as flash is automatically
                                        //locked after reboot
    canmessage_ack.content.response=ctrl_state_received_data_msg_count;
    canmessage_ack.content.addr=(uint16_t)address_to_flash;
    //Direct CAN send
    can_send_message(MSG_ACK_ID,MSG_ACK_DLC,canmessage_ack.data);
    ctrl_state_flush_to_flash=false;
  }
  if(ctrl_state_data_transfer_started==false){
    blocks_flashed=0;
  }
}

///
/// Copy ram_flash_write_block routine into RAM
///
inline void copy_flash_writer_to_ram() {
  get_ram_section_length();
  for (uint8_t i = 0; i < RAM_SEG_LEN; i++){
    f_ram[i] = ((uint8_t *) ram_flash_write_block)[i];
  }
}

int main(void) {
  clock_init();  //Yes, this is needed. We want to operate the CAN bus with a
                 //certain speed, therefore we need to know the Clock.

  //Check, if SW update shall be performed:
  //CRC mismatch or SW update requested by app: start SW update
  uint16_t current_crc=config_calculate_app_crc();
  uint16_t reqd_crc=config_get_app_value(BOOTLOADER_APP_CRC_ADDR);
  if((current_crc==reqd_crc)&&(config_get_sw_update_requested()==false)){
    __asm jp APP_START_ADDR __endasm;
  }
  else{
    //Perform SW update
    eeprom_init();  //we need this init only for write
    config_change_to_bootloader_mode();
    copy_flash_writer_to_ram();
    //Init all needed peripherals
    can_init();
    canmessages_init();
    ctrl_state_init();
    can_interrupt_enable();

    canmessages_send_status_message();
    while(ctrl_state_start_app_requested==false){
      bootloader_task();
    }
    //after update is finished, try to start application
    //Reset SW update request
    config_set_sw_update_requested(false);
    // Reboot device
    // It activates the Window Watchdog, which resets all because its seventh bit is null.
    // See page 132 of RM0016 (STM8S and STM8AF microcontroller family) for more details.
    WWDG->CR=0x80;
  }
  return 0;
}
