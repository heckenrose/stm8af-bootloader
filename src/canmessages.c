////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2021, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
// This is the list of CAN messages.
//

#include <appconfig.h>
#include <can.h>
#include "canmessages.h"
#include "config.h"
#include "ctrl_state.h"

canmessage_status_t canmessage_status;
canmessage_ack_t canmessage_ack;
canmessage_command_t canmessage_command;
canmessage_data_t canmessage_data;

#define MSG_COUNT_RX 2
#define RX_POS_COMMAND 0
#define RX_POS_DATA 1

canmessage_rx_config rx_list[MSG_COUNT_RX];

void canmessages_init(void){
  //Init RX list
  //Config message
  rx_list[RX_POS_COMMAND].id=MSG_CONFIG_ID;
  rx_list[RX_POS_COMMAND].dlc=MSG_CONFIG_DLC;
  rx_list[RX_POS_COMMAND].message=&((void*)(canmessage_command));
  rx_list[RX_POS_COMMAND].handled=true; //As we did not received an RX message, we
  //threat it as "all RX messages handled"
  //Data message
  rx_list[RX_POS_DATA].id=MSG_DATA_ID;
  rx_list[RX_POS_DATA].dlc=MSG_DATA_DLC;
  rx_list[RX_POS_DATA].message=&((void*)(canmessage_data));
  rx_list[RX_POS_DATA].handled=true; //As we did not received an RX message, we
  //threat it as "all RX messages handled"

  //Init message content
  canmessage_status.content.bl_version=SW_VERSION;
  canmessage_status.content.app_end_addr=config_get_app_value(BOOTLOADER_APP_END_ADDR);
}

void canmessages_send_status_message(void){
  canmessage_status.content.crc=config_calculate_app_crc();
  can_send_message(MSG_STATUS_ID,MSG_STATUS_DLC,canmessage_status.data);
}

//Store the received CAN messages. This is the Interrupt-handler for CAN-RX.
void canmessages_rx_store(void){
  uint8_t datapos,msgnum;
  canmessage_rx_t *tmpmsg;

  if(CAN_GetReceivedRTR()==CAN_RTR_Data){
    //Now iterate over rx list
    for(msgnum=0;msgnum<MSG_COUNT_RX;++msgnum){
      if(CAN_GetReceivedId()==rx_list[msgnum].id){
        //Hm, found message drop bin
        tmpmsg=rx_list[msgnum].message;
        for(datapos=0;((datapos<CAN_GetReceivedDLC())&&(datapos<rx_list[msgnum].dlc));datapos++){
          tmpmsg->data[datapos]=CAN_GetReceivedData(datapos);
        }
        //Store metadata
        rx_list[msgnum].handled=false;
      }
    }
  }
}

//React on the received CAN messages
void canmessages_rx_handler(void){
  uint8_t i;

  //Now handle all the RX messages
  for(i=0;i<MSG_COUNT_RX;i++){
    //Only handle message, if not handled before.
    //This prevent doubled reaction on commands of simply stored RX messages
    if(rx_list[i].handled==false){
      if(i==RX_POS_COMMAND){
        canmessages_handle_rx_config();
      }
      else if(i==RX_POS_DATA){
        canmessages_handle_rx_data();
      }
      rx_list[i].handled=true;
    }
  }
}

void canmessages_handle_rx_config(void){
  //Command 0x01 is currently not used.
  if(canmessage_command.content.config_command==0x02){
    //0x02=set application end address
    config_write_app_value(BOOTLOADER_APP_END_ADDR,canmessage_command.app_content.app_param);
    canmessage_status.content.app_end_addr=config_get_app_value(BOOTLOADER_APP_END_ADDR);
  }
  else if(canmessage_command.content.config_command==0x03){
    //0x03=set application CRC
    config_write_app_value(BOOTLOADER_APP_CRC_ADDR,canmessage_command.app_content.app_param);
  }
  else if(canmessage_command.content.config_command==0x04){
    //0x04=Start application
    ctrl_state_start_app_requested=true;
  }
  else if(canmessage_command.content.config_command==0x05){
    //0x05 = Request status message
    canmessages_send_status_message();
  }
  else if(canmessage_command.content.config_command==0x06){
    //0x06 = Start data transfer
    ctrl_state_received_data_msg_count=0;
    ctrl_state_data_transfer_started=true;
  }
  else if(canmessage_command.content.config_command==0x07){
    //0x07 = End data transfer.
    ctrl_state_flush_to_flash=true;
    canmessages_send_status_message();
    ctrl_state_received_data_msg_count=0;
    ctrl_state_data_transfer_started=false;
  }
}

void canmessages_handle_rx_data(void){
  for(uint8_t i=0;i<rx_list[RX_POS_DATA].dlc;++i){
    ctrl_state_flash_data[((ctrl_state_received_data_msg_count%16)*8)+i]=canmessage_data.data[i];
  }
  ++ctrl_state_received_data_msg_count;
  if((ctrl_state_received_data_msg_count%16)==0){
    ctrl_state_flush_to_flash=true;
  }
}
