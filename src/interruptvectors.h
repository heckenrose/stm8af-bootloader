////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2021, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef INTERRUPTVECTORS_H
#define INTERRUPTVECTORS_H

#include "canmessages.h"

// Interrupt numbers
#define TLI_IRQ 0
#define AWU_IRQ 1
#define CLK_IRQ 2
#define EXTI_PORT_A_IRQ 3
#define EXTI_PORT_B_IRQ 4
#define EXTI_PORT_C_IRQ 5
#define EXTI_PORT_D_IRQ 6
#define EXTI_PORT_E_IRQ 7
#define CAN_RX_IRQ 8
#define CAN_TX_IRQ 9
#define SPI_EOT_IRQ 10
#define TIM1_OVR_UIF_IRQ 11
#define TIM1_CAPCOM_IRQ 12
#define TIM2_OVR_UIF_IRQ 13
#define TIM2_CAPCOM_IRQ 14
#define TIM3_OVR_UIF_IRQ 15
#define TIM3_CAPCOM_IRQ 16
#define USART_TX_IRQ 17
#define USART_RX_IRQ 18
#define I2C_IRQ 19
#define LIN_TX_IRQ 20
#define LIN_RX_IRQ 21
#define ADC2_IRQ 22
#define TIM4_OVR_UIF_IRQ 23
#define EEPROM_IRQ 24 //Nicht benutzen
#define NONHANDLED_IRQ 25 //Nicht benutzen

//
// The interrupt vector table.
// Interrupt vectors must be implemented in the same translation unit (file) that implements main()
//  and the __interrupt attribute is used to specify their IRQ number (which becomes an offset into
//  the interrupt vector table).
//  The interrupt vector table is placed at the start of the program (0x8000 by default, or
//  whatever is set by using the --code-loc option).
// Relocation:
//  As such, interrupt 28 is 4 * 28 + 8 or 120 which gives us the offset 0x78 and, if the
//  application starts at 0x8400 the redirected vector is at 0x8478
//

#define DEFAULT_RELOC_HANDLER(IRQ_NUMBER) \
  __asm jpf APP_START_ADDR+4*IRQ_NUMBER+8 __endasm;

#define RELOC_IRQ(IRQ_NUMBER) \
void IRQ_NUMBER##HANDLER(void) __interrupt(IRQ_NUMBER)__naked{ \
  DEFAULT_RELOC_HANDLER(IRQ_NUMBER) \
}

RELOC_IRQ(TLI_IRQ)
RELOC_IRQ(AWU_IRQ)
RELOC_IRQ(CLK_IRQ)
RELOC_IRQ(EXTI_PORT_A_IRQ)
RELOC_IRQ(EXTI_PORT_B_IRQ)
RELOC_IRQ(EXTI_PORT_C_IRQ)
RELOC_IRQ(EXTI_PORT_D_IRQ)
RELOC_IRQ(EXTI_PORT_E_IRQ)
RELOC_IRQ(CAN_TX_IRQ)
RELOC_IRQ(SPI_EOT_IRQ)
RELOC_IRQ(TIM1_OVR_UIF_IRQ)
RELOC_IRQ(TIM1_CAPCOM_IRQ)
RELOC_IRQ(TIM2_OVR_UIF_IRQ)
RELOC_IRQ(TIM2_CAPCOM_IRQ)
RELOC_IRQ(TIM3_OVR_UIF_IRQ)
RELOC_IRQ(TIM3_CAPCOM_IRQ)
RELOC_IRQ(USART_TX_IRQ)
RELOC_IRQ(USART_RX_IRQ)
RELOC_IRQ(I2C_IRQ)
RELOC_IRQ(LIN_TX_IRQ)
RELOC_IRQ(LIN_RX_IRQ)
RELOC_IRQ(ADC2_IRQ)
RELOC_IRQ(TIM4_OVR_UIF_IRQ)
RELOC_IRQ(EEPROM_IRQ)
RELOC_IRQ(NONHANDLED_IRQ)

void CAN_RX_Handler(void) __interrupt(CAN_RX_IRQ){
  if((*(volatile uint8_t*)(BOOTLOADER_IVT_RELOC_ADDR))!=0x00){
    DEFAULT_RELOC_HANDLER(CAN_RX_IRQ)
  }
  else{
    //The CAN RX handler used by Bootloader.
    CAN_Receive();
    canmessages_rx_store();
  }
}


#endif //INTERRUPTVECTORS_H
