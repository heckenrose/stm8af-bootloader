////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2021, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////

#include <stm8s.h>
#include <stm8s_can.h>
#include "appconfig.h"
#include "can.h"

void can_init(void){
  CAN_InitStatus_TypeDef status = CAN_InitStatus_Failed;

  // Filter Parameters
  CAN_FilterNumber_TypeDef CAN_FilterNumber;
  FunctionalState CAN_FilterActivation;
  CAN_FilterMode_TypeDef CAN_FilterMode;
  CAN_FilterScale_TypeDef CAN_FilterScale;
  uint8_t CAN_FilterID1;
  uint8_t CAN_FilterID2;
  uint8_t CAN_FilterID3;
  uint8_t CAN_FilterID4;
  uint8_t CAN_FilterIDMask1;
  uint8_t CAN_FilterIDMask2;
  uint8_t CAN_FilterIDMask3;
  uint8_t CAN_FilterIDMask4;

  // Init Parameters
  CAN_MasterCtrl_TypeDef CAN_MasterCtrl;
  CAN_Mode_TypeDef CAN_Mode;
  CAN_SynJumpWidth_TypeDef CAN_SynJumpWidth;
  CAN_BitSeg1_TypeDef CAN_BitSeg1;
  CAN_BitSeg2_TypeDef CAN_BitSeg2;
  uint8_t CAN_Prescaler;

  // CAN register init
  CAN_DeInit();
  // CAN  init
  CAN_MasterCtrl=CAN_MasterCtrl_AutoBusOffManagement;
  CAN_Mode = CAN_Mode_Normal;
  CAN_SynJumpWidth = CAN_SynJumpWidth_1TimeQuantum;
  //Set Speed
  //can_calculate_speedparameter();
  //CAN_BitSeg1 = m_prescaler.bs1;   //-1;
  //CAN_BitSeg2 = m_prescaler.bs2;   //-1;
  //CAN_Prescaler = m_prescaler.brp;  //No need to move
#if USE_MCU_CLOCK_MHZ == 16
  CAN_Prescaler = 2;  //No need to move
  CAN_BitSeg1 = CAN_BitSeg1_8TimeQuantum;
  CAN_BitSeg2 = CAN_BitSeg2_7TimeQuantum;   //-1;
#elif USE_MCU_CLOCK_MHZ == 8
  CAN_Prescaler = 1;  //No need to move
  CAN_BitSeg1 = CAN_BitSeg1_8TimeQuantum;
  CAN_BitSeg2 = CAN_BitSeg2_7TimeQuantum;   //-1;
#elif USE_MCU_CLOCK_MHZ == 4
  CAN_Prescaler = 1;  //No need to move
  CAN_BitSeg1 = CAN_BitSeg1_6TimeQuantum;
  CAN_BitSeg2 = CAN_BitSeg2_1TimeQuantum;   //-1;
#elif USE_MCU_CLOCK_MHZ == 2
  CAN_Prescaler = 1;  //No need to move
  CAN_BitSeg1 = CAN_BitSeg1_2TimeQuantum;
  CAN_BitSeg2 = CAN_BitSeg2_1TimeQuantum;   //-1;
#endif
  status = CAN_Init(CAN_MasterCtrl, CAN_Mode, CAN_SynJumpWidth, CAN_BitSeg1, CAN_BitSeg2, CAN_Prescaler);

  // CAN filter init: allow all config messages
  CAN_FilterNumber = CAN_FilterNumber_0;  //=Filter bank
  CAN_FilterActivation = ENABLE;
  //CAN_FilterMode = CAN_FilterMode_IdList; //CAN_FilterMode_IdMask;
  CAN_FilterMode = CAN_FilterMode_IdMask;
  CAN_FilterScale = CAN_FilterScale_32Bit;
  //Ich will hier alle 07[E|F]y-Nachrichten haben
  uint16_t single_id=0x007E;
  //Jetzt noch die Basis-ID reinodern
  //single_id|=SENSOR_ID;
  CAN_FilterID1=(uint8_t)(((uint16_t)single_id)>>3);   //Bits 10:3 des Standard-Identifiers
  CAN_FilterID2=(uint8_t)(((uint16_t)single_id)<<5);   //Bits 2:0 des Standard-Identifiers + 5 weitere binäre Nullen
  //CAN_FilterID1=0;
  //CAN_FilterID2=0;
  CAN_FilterID3=0;
  CAN_FilterID4=0;
  //Jetzt den Kram ausmaskieren: Alle Bits, die beachtet werden sollen, sind 1, die
  //anderen 0
  uint16_t filter_mask=0x00FE; //Das letzte Bit soll nicht beachtet werden, daher E statt F.
  CAN_FilterIDMask1=(uint8_t)(((uint16_t)filter_mask)>>3);   //Bits 10:3 des Standard-Identifiers
  CAN_FilterIDMask2=(uint8_t)(((uint16_t)filter_mask)<<5);   //Bits 2:0 des Standard-Identifiers + 5 weitere binäre Nullen
  CAN_FilterIDMask3=0xFF;
  CAN_FilterIDMask4=0xFF;
  CAN_FilterInit(CAN_FilterNumber, CAN_FilterActivation, CAN_FilterMode,
                 CAN_FilterScale,CAN_FilterID1, CAN_FilterID2, CAN_FilterID3,
                 CAN_FilterID4,CAN_FilterIDMask1, CAN_FilterIDMask2,
                 CAN_FilterIDMask3, CAN_FilterIDMask4);
}

void can_interrupt_enable(void){
  // Enable Fifo message pending interrupt
  // Message reception is done by CAN_RX ISR
  CAN_ITConfig(CAN_IT_FMP|CAN_IT_WKU, ENABLE);
  enableInterrupts();
}

bool can_send_message(uint32_t id,uint8_t dlc,uint8_t *data){
  CAN_TxStatus_TypeDef status = CAN_TxStatus_Failed;
  bool retval = false;

  status = CAN_Transmit(id,CAN_Id_Standard,CAN_RTR_Data,dlc,data);

  if((status != CAN_TxStatus_Failed) && (status != CAN_TxStatus_NoMailBox)) retval=true;
  return retval;
}
