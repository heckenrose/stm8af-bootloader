////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2021, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
// Configurative constants
//

#ifndef APPCONFIG_H
#define APPCONFIG_H

#define SW_VERSION 2

//==========================================================
// Speed definitions
//==========================================================
//MCU clock step
#define USE_MCU_CLOCK_MHZ 16
//Default CAN speed in kbit/s
#define CAN_DEFAULT_SPEED 500

//==========================================================
// CAN Message config part.
//
//==========================================================
//Die RX-Nachrichten sollten so sein:
//0x07E: Config
//0x07F: Daten
//Die TX-Nachrichten sollten so sein:
//0x08E: Status (Ident usw.)
//0x08F: Ack

//Status
#define MSG_STATUS_ID 0x08E
#define MSG_STATUS_DLC 5
//Ack
#define MSG_ACK_ID 0x08F
#define MSG_ACK_DLC 4

//Config
#define MSG_CONFIG_ID 0x07E
#define MSG_CONFIG_DLC 8
//Data
#define MSG_DATA_ID 0x07F
#define MSG_DATA_DLC 8


//==========================================================
// EEPROM Adresses.
// The whole EEPROM is 2048 bytes long
//==========================================================
#define BOOTLOADER_EEPROM_BASE_ADDR 0x4000
#define BOOTLOADER_IVT_RELOC_ADDR BOOTLOADER_EEPROM_BASE_ADDR
#define BOOTLOADER_IVT_RELOC_CHUNK_LEN 1
#define BOOTLOADER_SW_UPDATE_ADDR BOOTLOADER_IVT_RELOC_ADDR+BOOTLOADER_IVT_RELOC_CHUNK_LEN
#define BOOTLOADER_SW_UPDATE_CHUNK_LEN 1
#define BOOTLOADER_APP_END_ADDR BOOTLOADER_SW_UPDATE_ADDR+BOOTLOADER_SW_UPDATE_CHUNK_LEN
#define BOOTLOADER_APP_ADDR_CHUNK_LEN 2
#define BOOTLOADER_APP_CRC_ADDR BOOTLOADER_APP_END_ADDR+BOOTLOADER_APP_ADDR_CHUNK_LEN
#define BOOTLOADER_APP_CRC_CHUNK_LEN 2

#define APP_START_ADDR 0x9000

#endif
