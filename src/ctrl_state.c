////////////////////////////////////////////////////////////////////////////////
//
// BSD 3-Clause License
//
// Copyright (c) 2021, Dirk Neumann
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
////////////////////////////////////////////////////////////////////////////////
//
// This is the control state. The state is set and checked by various actors.
//

#include "ctrl_state.h"

volatile bool ctrl_state_start_app_requested;
volatile bool ctrl_state_can_rx_message_pending;
volatile bool ctrl_state_data_transfer_started;
volatile bool ctrl_state_flush_to_flash;
volatile uint16_t ctrl_state_received_data_msg_count;

volatile uint8_t ctrl_state_flash_data[FLASH_BLOCK_SIZE];

void ctrl_state_init(void){
  ctrl_state_start_app_requested=false;
  ctrl_state_can_rx_message_pending=false;
  ctrl_state_data_transfer_started=false;
  ctrl_state_received_data_msg_count=0;
  ctrl_state_flush_to_flash=false;
  ctrl_state_clear_flash_data();
}

void ctrl_state_clear_flash_data(void){
  for(uint8_t i=0;i<FLASH_BLOCK_SIZE;++i) ctrl_state_flash_data[i]=0;
}
