## Why?
As it came to the point that I needed a bootloader which can update my application
via CAN bus, I planned to use the ST bootloader which is decribed in the UM0560:
User Manual for STM8 bootloader

https://www.st.com/resource/en/user_manual/cd00201192-stm8-bootloader-stmicroelectronics.pdf

But then, I read on page 9 that the CAN is initialized with 125 kbps. The CAN
transceiver I used did not support this speed, because it is a CAN high speed only
transceiver which made data rated from 250k and more. Sure, the ST bootloader
can be commanded to use other data rates, but how to command it, when the CAN
communication does not work?
As an addition, my board had no HSE mounted, which is needed for the CAN boot mode.
So I needed to write a bootloader which comes without these decisions.

The original ST bootloader is located at `0x6000`, whereas the user code starts
at `0x8000`. It cannot be avoided that we need some of this space for the
bootloader. Hopefully it is not too much.

## Boot mode

The ST bootloader checks on address `0x8000` if there is `0x82` or `0xAC`. If not,
it is time for bootloader. As another option, the ST bootloader looks at option
bytes, which are set be application to singalize that a software update has to be
performed. We use a similar approach, but with EEPROM instead of option bytes.
Therefore we use the first 2 bytes of EEPROM: The first (at `0x4000`) signalizes,
if interrupt vectors are to be relocated (more on that later) or not. The
second byte (`0x4001`) signalizes, if there is a software update requested by
application. The general sequence to start is:

1. Is `0x4001 == 0x01?` If yes, jump over to 3.
2. Is the CRC of the app area correct? If yes, jump over to 5.
3. Initialize CAN and wait for commands
4. If command== `0x04`: Is CRC of app area correct? If yes jump over to 5.
5. Start application by jumping to app start address.

It needs some cooperation with application. If the app is started, it has to
take over the interrupt vector table by writing `0x01` to EEPROM address `0x4000`.
Then it has to take back the app update request by writing `0x00` to EEPROM address
`0x4001`. After that, the application can initialize all peripherals it needs
and start its regular behaviour, e.g. measuring values.
The application may start update process again by writing `0x01` to EEPROM address
`0x4001` and then reboot.

## CRC check
To determine, if the resident application is correct, the bootloaders performs
a CRC check. It is CRC16-CCITT. The area to check starts at the application start
address (`0x9000`) and ends at the application end address. This end address is
expected to be in EEPROM starting at address `0x4002`. The format is big endian.
The CRC to be compared with is expected to start at EEPROM address `0x4004` and
is also noted in big endian. But how are these values written into EEPROM?
There is a command for each value to write, which is executed by bootloader.
So the update client has to provide the application end address and the application
CRC via CAN commands to bootloader.

## Interrupt vector relocation
As the STM8 expects its interrupt vectors starting at address `0x8000`, the
bootloader manages these functions. The application has its own interrupt vector
table, which starts at the application start address. These interrupt handlers
cannot be executed by nature, because all interrupt handlers are expected to
start at `0x8000` plus a specific offset for each interrupt. Each interrupt vector
table entry is formed as `INT` or `JPF` instruction:

`INT  82 ee ww bb`
or
`JPF  AC ee ww bb`

So the bootloader interrupt vectors have to jump to the application interrupt 
vector addresses. Such an address is calculated as follows:
`APP_START_ADDR + 4*IRQ_NUMBER + 8`.

As long as we do not use interrupts in bootloader, each interrupt vector table
entry can consist such an unconditional jump. But we use CAN RX interrupt to
get program data via CAN, so we have to use conditional interrupt vector relocation.
CAN RX is the only interrupt which is used by bootloader, so all other vectors
can simply be relocated, the CAN RX interrupt handler has to check first, if
it shall be executed with bootloader code. For that reason, we use the EEPROM
address `0x4000`. Reading from EEPROM takes 0 wait states, which makes us happy
enough.
More on interrupt vector relocation can be found at the STM8 bootloader
project, which I used as an idol:
* https://github.com/lujji/stm8-bootloader.git
* https://lujji.github.io/blog/serial-bootloader-for-stm8/

## Scheduling
The bootloader does not use timer interrupts, because they may occur often, and
the conditional IVT relocation may use too much MCU cycles, or other time
measuring ways, and therefore no central scheduling. It is purely driven by CAN
RX interrupt. It just reacts on each CAN message it receives. If enough data
messages occured, a block is written to flash, if other commands arrive, they
are handled.

## EEPROM overview

|  Address  | Length |  Content                  |
| --------- | ------ | ------------------------- |
| `0x4000`  |  1     | Relocate IVT              |
| `0x4001`  |  1     | Perform SW Update         |
| `0x4002`  |  2     | Application end address   |
| `0x4004`  |  2     | Application CRC           |

* Relocate IVT: `0x01`= Relocate, `0x00`= No Relocation. Written by application.
* Perform SW Update: `0x01`=Perform update, `0x00`=No SW update. Written by
  application
* Application end address: 16 Bit, Big Endian
* Application CRC: 16 Bit, CCITT, Big Endian


## CAN Messages

### RX
* `0x07E` Config/Command
* `0x07F` Data

#### `0x07E` Config/Command
The first byte is the command, followed by parameters.

| Command      | Meaning                       | Parameters                            |
| ------------ | ----------------------------- | ------------------------------------- |
| `0x02 ABCD`  | set app end address           | `0xABCD`: address, 16 Bit, Big Endian |
| `0x03 ABCD`  | set app CRC                   | `0xABCD`: CRC, 16 Bit, Big Endian     |
| `0x04`       | Quit Bootloader and start app | none                                  |
| `0x05`       | Request status message        | none                                  |
| `0x06`       | Start data transfer           | none                                  |
| `0x07`       | End data transfer             | none                                  |


Some contemplation about commands:
* `0x04`: If the application cannot be started (e.g. CRC mismatch), we restart in BL mode.
* `0x05`: The status message is sent as response to this request.


#### `0x07F` Data
A data message contains up to 8 data bytes. The order is rising. After 16 messages,
the bootloader writes the block (128 bytes) into flash and responds with ACK
message, if successful. The programming host therefore has to wait after 16
data messages to receive an ACK message. 

If there is not enogh data to fill
128 bytes, the programming host has to shorten the last data message to the number
of bytes left (e.g. 3) and then send a command `End data transfer`. The bootloader
writes the bytes into flash and sends an ACK message. 


### TX
* `0x8E` Status
* `0x8F` ACK

#### `0x8E` Status

```
+---------+---------+--------+---------+--------+
| Byte 0  | Byte 1..2        | Byte 3..4        |
+---------+---------+--------+---------+--------+
| Version | App end address  | App CRC          |
+---------+---------+--------+---------+--------+
```
APP CRC is the calculated CRC over the app region.

#### `0x8F` ACK
```
+---------+---------+--------+---------+
| Byte 0..1         | Byte 2..3        |
+---------+---------+--------+---------+
| Data messages     | Flash block start|
| received          | address          |
+---------+---------+--------+---------+
```

## Message time
At bootloader start, it sends a single CAN status message, if the application
is not started.

## Restrictions
Currently the bootloader does not support data write and read beyond address
0xFFFF. To achieve this, there is some extra work to do. There is already a
feature request for SDCC to support this:
https://sourceforge.net/p/sdcc/feature-requests/560/
Up to now, the application can only be 28KB, if the app start address is
`0x9000`.

## Size of bootloader
Currently the app start address is `0x9000`, meaning the bootloader has got
4 KB of user flash memory. But in fact, it uses 2518 bytes, which means we may
change the app start address to `0x8C00` or even to `0x8A00`, which gives us more
than an extra KB of app size. But `0x9000` is more readable and we have some space
left for the bootloader to write and test new features.

Because SDCC currently does not eliminate all unused functions at link time,
I did so by commenting out unneeded functions of the STM standard peripheral
library modules. There is a feature request for SDCC to do so:
https://sourceforge.net/p/sdcc/feature-requests/635/

I also commented out some code lines in the CAN functions of
the STM standard peripheral library. Here you have to look carefully, if you
would need it some day. This concerns CAN filter settings. For the used CAN RX
messages `0x7E` and `0x7f` it works, but if you would do some other things and
wish to have more CAN RX messages, you would have to remove the comment chars
at the respective lines.

The size of bootloader can be further reduced by handcraft some things in
assembler. An article can be found here:
https://lujji.github.io/blog/mixing-c-and-assembly-on-stm8/

## Evironment
This bootloader is tested on STM8AF5288 on ST Discovery Board and on a custom
board based ob STM8AF5286.

## Compiling
This bootloader is tested to be compileable on Debian Buster machine with SDCC
installed from source (version 4.1.0), cmake and make installed from Debian
standard package repository. Steps:

```
mkdir build
cd build
cmake ..
make
```

This builds a file called stm8af_bootloader.ihx, which can be flashed to target,
e.g. with STM8Flash: https://github.com/vdudouyt/stm8flash

```
stm8flash -c stlinkv2 -p stm8af5288t -w stm8af_bootloader.ihx
```
